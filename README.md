# Product Landing Page Example

Une page exemple pour présenter ses produits réalisée en HTML/CSS. Je vous présent une pâtisserie de la Guyane française : les Comtesses

## Installation

Vous pouvez cloner le projet en utilisant git clone

```bash
git clone https://gitlab.com/mateusbubono/product-landing-page.git
```

## Usage

Vous pouvez ouvrir le fichier index.html après avoir cloné le projet ou vous rendre directement à l'adresse suivante : 

https://mateusbubono.gitlab.io/product-landing-page/